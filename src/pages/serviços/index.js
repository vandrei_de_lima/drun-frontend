import React, { Component } from "react";
import { Container } from "../home/styled";
import Header from "../../components/Header";
import SideDrawer from "../../components/SideDrawer/index";
import FundoEscuro from "../../components/fundoEscuro/index";
import RodaPé from "../../components/rodaPé";
import { ConteinerServico } from "./styled";
import Foto from "../../style/img/Imagem3.png";

class Servico extends Component {
  state = {
    abrirMenu: false,
  };

  abrirMenu = () => {
    this.setState((prevState) => {
      return { abrirMenu: !prevState.abrirMenu };
    });
  };

  fecharMenu = () => {
    this.setState({ abrirMenu: false });
  };

  render() {
    let fundoEscuro;

    if (this.state.abrirMenu) {
      fundoEscuro = <FundoEscuro click={this.fecharMenu} />;
    }
    return (
      <Container>
        <Header abrir={this.abrirMenu} />
        <SideDrawer show={this.state.abrirMenu} />
        {fundoEscuro}
        <ConteinerServico>
          <main>
            <div id="Sobre">
              <h1>Serviços</h1>
              <ul className="Servico">
                <li>Primeira Habilitação</li>
                <li>Alteração de Dados</li>
                <li>Curso de Atualização</li>
                <li>Aulas para Habilitados</li>
                <li>Renovação</li>
                <li>Reciclagem</li>
                <li>Segunda Via</li>
                <li>PID</li>

                <li>Adição de Categoria</li>
              </ul>
            </div>
            <div id="Sobre1">
              <img src={Foto} alt={Foto} />
            </div>
          </main>
        </ConteinerServico>
        <RodaPé />
      </Container>
    );
  }
}

export default Servico;
