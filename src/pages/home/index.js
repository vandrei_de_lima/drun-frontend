import React, { Component } from "react";
import { Container } from "../home/styled";
import Header from "../../components/Header";
import SideDrawer from "../../components/SideDrawer/index";
import FundoEscuro from "../../components/fundoEscuro/index";
import RodaPé from "../../components/rodaPé";
import { CorpoHome } from "./styled";
import FotoCentro from "../../style/img/logo-branco-amarelo.png";
import { BiCalendarCheck } from "react-icons/bi";
import { BiIdCard } from "react-icons/bi";
import { AiFillCar } from "react-icons/ai";
import { GoPerson } from "react-icons/go";

class Home extends Component {
  state = {
    abrirMenu: false,
  };

  abrirMenu = () => {
    this.setState((prevState) => {
      return { abrirMenu: !prevState.abrirMenu };
    });
  };

  fecharMenu = () => {
    this.setState({ abrirMenu: false });
  };

  render() {
    let fundoEscuro;

    if (this.state.abrirMenu) {
      fundoEscuro = <FundoEscuro click={this.fecharMenu} />;
    }
    return (
      <Container>
        <Header abrir={this.abrirMenu} />
        <SideDrawer show={this.state.abrirMenu} />
        {fundoEscuro}
        <CorpoHome>
          <section className="flex">
            <div className="eslogam">
              <img src={FotoCentro} alt="logo" />
              <p>
                Aquí teoria vira <b>prática!</b>
              </p>
            </div>
          </section>
          <section className="flex">
            <div>
              <BiCalendarCheck size={200} />
              <p>Compromentimento nas entregas</p>
            </div>
            <div>
              <BiIdCard size={200} />
              <p>
                Sua Carteira em mãos com <b>agilidade</b>
              </p>
            </div>
            <div>
              <AiFillCar size={200} />
              <p>Estrutura completa</p>
            </div>
            <div>
              <GoPerson size={200} />
              <p>Profisionais qualificados</p>
            </div>
          </section>
        </CorpoHome>
        <div className="rodaPé">
          <RodaPé />
        </div>
      </Container>
    );
  }
}

export default Home;
