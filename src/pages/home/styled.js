import styled from "styled-components";

export const Calculadora = styled.div`
  margin-top: 64px;
`;
export const Container = styled.div`
  overflow-y: hidden;
  .rodaPé {
    position: relative;
    margin-top: 18%;
  }
  @media (max-width: 768px) {
    margin-top: 30px;
    .rodaPé {
      margin-top: 18%;
    }
  }
`;

export const CorpoHome = styled.div`
  margin-top: 64px;

  img {
    max-width: 100%;
    display: block;
  }

  h1 {
    text-align: center;
  }

  .flex {
    display: flex;
    color: #fff;
    flex-wrap: wrap;
    max-width: 800px;
    margin: 0 auto;

    .eslogam {
      align-items: center;
      max-width: 500px;
      margin: 0 auto;
      text-align: center;
    }
  }

  .flex > div {
    flex: 1 1 250px;
    margin: 10px;
    text-align: center;
  }
`;
