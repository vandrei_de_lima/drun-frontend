import React, { Component } from "react";
import { Container } from "../home/styled";
import { ConteinerSobre } from "./styled";
import Header from "../../components/Header";
import SideDrawer from "../../components/SideDrawer/index";
import FundoEscuro from "../../components/fundoEscuro/index";
import RodaPé from "../../components/rodaPé";
import Foto from "../../style/img/Imagem1.png";

class Sobre extends Component {
  state = {
    abrirMenu: false,
  };
  abrirMenu = () => {
    this.setState((prevState) => {
      return { abrirMenu: !prevState.abrirMenu };
    });
  };

  fecharMenu = () => {
    this.setState({ abrirMenu: false });
  };

  render() {
    let fundoEscuro;

    if (this.state.abrirMenu) {
      fundoEscuro = <FundoEscuro click={this.fecharMenu} />;
    }
    return (
      <Container>
        <ConteinerSobre>
          <Header abrir={this.abrirMenu} />
          <SideDrawer show={this.state.abrirMenu} />
          {fundoEscuro}

          <main>
            {" "}
            <div id="Sobre">
              {" "}
              A <b>Drun</b> de hoje é o resultado de um projeto incrível que
              começou <b>anos atrás! </b>
              Com foco em um <b>trânsito mais seguro</b>, possui uma metodologia
              própria voltada ao
              <b> treinamento de habilitados</b>, oferecendo assim, todo o apoio
              e suporte necessários para uma
              <b>
                {" "}
                direção segura, confiante e prática.
                <br />
                <br />
                Agora{" "}
              </b>
              ,toma forma também como <b>Autoescola </b>, contando com uma frota
              de <b>veículos de primeira </b>
              linha, <b>instrutores </b>capacitados e especializados,
              <b>estrutura própria e ampla</b>, além de <b>oferecer </b>
              toda a <b>atenção que você precisa</b> num dos momentos mais
              importantes da sua vida. Unindo o histórico de{" "}
              <b>tradição à modernidade</b>, a Drun garante a exclusividade da
              empresa no mercado jaraguaense{" "}
              <b>colocando você no caminho certo</b>.
            </div>
            <div id="Sobre1">
              <img src={Foto} alt={Foto} />
            </div>
          </main>
          <div className="rodaPé">
            <RodaPé />
          </div>
        </ConteinerSobre>
      </Container>
    );
  }
}

export default Sobre;
