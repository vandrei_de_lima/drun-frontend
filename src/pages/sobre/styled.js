import styled from "styled-components";

export const ConteinerSobre = styled.div`
  align-items: center;

  main {
    margin-top: 64px;
    grid-gap: 20px;

    list-style: none;
    height: 500px;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));

    @media (min-width: 768px) {
      font-size: 15px;
      margin-top: 64px;
      padding: 1em;
      grid-template-columns: repeat(2, minmax(300px, 1fr));
    }

    @media (max-width: 768px) {
      margin-top: 30px;
    }
  }

  div#Sobre {
    color: #f5e61e;
    background: #07a4ea;
    height: 320px;

    @media (max-width: 768px) {
      margin-top: 30px;
      height: 480px;
    }

    margin: 10%;
    padding: 40px;
    border-radius: 2vw 5vw 2vw 5vw;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);

    text-align: left;

    @media (max-width: 768px) {
      margin-top: 100px;
      padding: 15px;
      margin: 10%;
    }

    .Servico {
      grid-gap: 4px;

      display: grid;
      grid-template-columns: repeat(2, minmax(150px, 1fr));

      @media (max-width: 768px) {
        margin-left: 10px;
        grid-gap: none;
        display: block;
        grid-template-columns: none;
      }

      li {
        list-style: circle;

        list-style-type: circle;
        padding: 10px;
      }

      .DescriçãoServiço {
        list-style-type: circle;
        color: #fff;
      }
    }
  }

  div#Sobre1 {
    color: #f5e61e;
    background: #07a4ea;

    height: 100%;
    margin: 10%;
    padding: 40px;
    border-radius: 2vw 5vw 2vw 5vw;

    text-align: left;

    @media (max-width: 768px) {
      margin-top: 100px;
      padding: 15px;
      margin: 10%;
    }

    img {
      width: 100%;
      border-radius: 4px;

      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }
  }
`;
