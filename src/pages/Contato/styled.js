import styled from "styled-components";

export const Container = styled.div`
  overflow-y: hidden;
  .rodaPé {
    position: relative;
    margin-top: 27%;
  }
  @media (max-width: 768px) {
    margin-top: 30px;

    .rodaPé {
      margin-top: 320px;
    }
  }
`;

export const ConteinerForm = styled.div`
  align-items: center;

  main {
    margin-top: 64px;
    grid-gap: 20px;

    list-style: none;
    height: 500px;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));

    div#Sobre {
      height: 460px;
    }

    div#Sobre1 {
      height: 350px;
    }

    @media (min-width: 768px) {
      font-size: 15px;
      margin-top: 64px;
      padding: 1em;
      grid-template-columns: repeat(2, minmax(300px, 1fr));
    }

    @media (max-width: 768px) {
      margin-top: 30px;
      div#Sobre {
        height: 400px;
      }
    }
  }

  form {
    display: flex;
    flex-direction: column;
    margin-top: 10px;

    input[type="text"],
    input[type="number"],
    input[type="tel"],
    textarea {
      font-size: 16px;
    }

    div#Total {
      margin: 15px;

      label {
        padding: 5px;
        color: #fff;
      }
    }

    Input {
      background: rgba(0, 0, 0, 0.2);
      border: 0;
      border-radius: 4px;
      height: 44px;
      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;
      letter-spacing: 1px;
      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }
    }

    Textarea {
      background: rgba(0, 0, 0, 0.2);
      border: 0;
      border-radius: 4px;

      padding: 0 15px;
      color: #fff;
      margin: 0 0 10px;

      letter-spacing: 1px;

      padding: 10px;
      line-height: 1.5;

      &::placeholder {
        color: rgba(255, 255, 255, 0.7);
      }
    }

    button {
      margin: 5px 0px 20px;
      height: 44px;
      background: #f5e61e;
      font-weight: bold;
      color: #07a4ea;
      border: 0px;
      border-radius: 4px;
      font-size: 16px;
      transition: background 0.2;
    }
    a {
      color: #fff;
      margin-top: 20px;
      font-size: 16px;
      opacity: 0.8;

      &:hover {
        opacity: 1;
      }
    }
  }

  div#Sobre {
    color: #f5e61e;
    background: #07a4ea;

    margin: 10%;
    padding: 40px;
    border-radius: 2vw 5vw 2vw 5vw;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);

    text-align: left;

    @media (max-width: 768px) {
      margin-top: 100px;
      padding: 15px;
      margin: 10%;
    }

    .Servico {
      grid-gap: 4px;

      display: grid;
      grid-template-columns: repeat(2, minmax(150px, 1fr));

      @media (max-width: 768px) {
        margin-left: 10px;
        grid-gap: none;
        display: block;
        grid-template-columns: none;
      }

      li {
        list-style: circle;

        list-style-type: circle;
        padding: 10px;
      }

      .DescriçãoServiço {
        list-style-type: circle;
        color: #fff;
      }
    }
  }

  div#Sobre1 {
    color: #f5e61e;
    background: #07a4ea;

    height: 100%;
    margin: 10%;
    padding: 40px;
    border-radius: 2vw 5vw 2vw 5vw;

    text-align: left;

    @media (max-width: 768px) {
      margin-top: 100px;
      padding: 15px;
      margin: 10%;
    }

    img {
      width: 100%;
      border-radius: 4px;

      box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.2);
    }
  }
`;
