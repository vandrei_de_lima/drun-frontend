import React, { Component } from "react";
import { Container } from "./styled";
import Header from "../../components/Header";
import SideDrawer from "../../components/SideDrawer/index";
import FundoEscuro from "../../components/fundoEscuro/index";
import RodaPé from "../../components/rodaPé";
import { ConteinerForm } from "./styled";
import Foto from "../../style/img/Imagem1.png";
import { Form, Input, Textarea } from "@rocketseat/unform";
import api from "../../config/config";
import * as Yup from "yup";

const schema = Yup.object().shape({
  Email: Yup.string().required("Obrigatório!"),

  Assunto: Yup.string().required("Obrigatório!"),
  Mensagem: Yup.string().required("Obrigatório!"),
});

class Info extends Component {
  state = {
    abrirMenu: false,
    FormularioEnviado: false,
  };

  abrirMenu = () => {
    this.setState((prevState) => {
      return { abrirMenu: !prevState.abrirMenu };
    });
  };

  fecharMenu = () => {
    this.setState({ abrirMenu: false });
  };

  handleSubmit = ({ Email, Assunto, Mensagem }) => {
    let data = {
      email: Email,
      assunto: Assunto,
      mensagem: Mensagem,
    };

    api
      .post("/api/formulario", data)
      .then((res) => {
        this.setState({ FormularioEnviado: true });
      })
      .catch(() => {
        console.log("mensagem nao enviada");
      });

    setTimeout(function () {
      document.getElementById("Email").value = "";
      document.getElementById("Assunto").value = "";
      document.getElementById("Mensagem").value = "";
    }, 2500);
  };

  render() {
    let fundoEscuro;
    let botao = <button type="submit">Enviar</button>;

    console.log(this.state);

    if (this.state.FormularioEnviado) {
      botao = <button>Enviado!</button>;
    }

    if (this.state.abrirMenu) {
      fundoEscuro = <FundoEscuro click={this.fecharMenu} />;
    }
    return (
      <Container>
        <Header abrir={this.abrirMenu} />
        <SideDrawer show={this.state.abrirMenu} />
        {fundoEscuro}

        <ConteinerForm>
          <main>
            {" "}
            <div id="Sobre">
              <h4>Entre Em Contato</h4>
              <Form
                className="Formulario"
                onSubmit={this.handleSubmit}
                schema={schema}
              >
                <meta
                  name="viewport"
                  content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"
                />
                <Input
                  name="Email"
                  type="text"
                  size="20"
                  maxlength="30"
                  placeholder="Email"
                />
                <Input
                  name="Assunto"
                  type="text"
                  placeholder="Assunto"
                  size="10"
                  maxlength="20"
                />
                <Textarea
                  name="Mensagem"
                  placeholder="Mensagem"
                  rows="5"
                  cols="33"
                />
                {
                  //https://www.youtube.com/watch?v=o3eR0X91Ogs
                }

                {botao}
              </Form>
            </div>
            <div id="Sobre1">
              <img src={Foto} alt={Foto} />
            </div>
          </main>
        </ConteinerForm>
        <div className="rodaPé">
          <RodaPé />
        </div>
      </Container>
    );
  }
}

export default Info;
