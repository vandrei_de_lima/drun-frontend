import axios from "axios";

const api = axios.create({
  baseURL: "https://auto-escola-drun.herokuapp.com",
  withCredentials: false,
});

export default api;
