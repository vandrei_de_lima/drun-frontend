import React, { Component } from 'react';
import { ContainerSide } from './styled'

class SideDrawer extends Component {


  render() {
    let classEstilo = 'MenuLateral'

    if (this.props.show) {
      classEstilo = 'MenuLateral abrir'
    }
    return (
      <ContainerSide>
        <nav class={classEstilo}>
          <ul>
            <li><a href="/" className="botao">Home</a></li>
            <li><a href="/Sobre" className="botao">Sobre</a></li>
            <li><a href="/Serviços" className="botao">Serviços</a></li>
            <li> <a href="/Informacao" className="botao">Contato</a></li>
          </ul>
        </nav>
      </ContainerSide >
    )
  }
}
export default SideDrawer;
