import styled from "styled-components";

export const Container = styled.button`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  height: 25px;
  width: 36px;
  background: transparent;
  border: none;
  cursor: pointer;
  box-sizing: border-box;

  :focus {
    outline: none;
  }

  .linha {
    width: 30px;
    height: 2px;
    background-color: #f5e61e;
  }
`;
export const ContainerSide = styled.nav`
  .MenuLateral.abrir {
    transform: translateX(0);
  }

  .MenuLateral {
    height: 100%;
    border-radius: 4px;
    background: #f5e61e;
    box-shadow: 2px 0px 7px rgba(0, 0, 0, 0.5);
    position: fixed;
    top: 0;
    left: 0;
    width: 30%;
    max-width: 30%;
    max-width: 300px;
    transform: translateX(-100%);
    transition: transform 0.3s ease-out;

    @media (max-width: 800px) {
      width: 60%;
    }

    width: 100%;
    z-index: 200;

    ul {
      display: flex;
      flex-direction: column;
    }

    li {
      margin-top: 10px;
    }

    .botao {
      margin-top: 10px;
      margin: 0px 0px 0px 10px;
      cursor: pointer;
      font-weight: bold;
      font-size: 30px;
      background: #07a4ea;
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }
  }
`;
