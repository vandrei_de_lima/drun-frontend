import React, { Component } from 'react';
import { Container } from './styled'
import icone from '../../style/img/icone.png'

import DrawerToggleButton from '../SideDrawer/DrawerToogleButton'

class Header extends Component {


  render() {
    return (
      <Container >
        <nav className="navegation">
          <div className="icone">
            <DrawerToggleButton click={this.props.abrir} />

          </div>
          <div><img className="logo" src={icone} alt="Ondog"></img></div>
          <div className="espaco" />
          <div className="navegation_itens">
            <ul>
              <li><a href="/" className="botao">Home</a></li>
              <li><a href="/Sobre" className="botao">Sobre</a></li>
              <li><a href="/Serviços" className="botao">Serviços</a></li>
              <li> <a href="/Informacao" className="botao">Contato</a></li>
            </ul>
          </div>
        </nav>
      </Container>
    );
  }
}

export default Header;
