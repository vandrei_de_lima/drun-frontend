import styled from "styled-components";

export const Container = styled.div`
  color: #07a4ea;
  background: #f5e61e;
  padding: 0.8em;
  position: absolute;
  bottom: 0px;
  width: 100%;
  align-items: center;
  text-align: center;
  box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.5);
  font-size: 14px;

  @media (max-width: 768px) {
    padding: 0.8em;
    font-size: 15px;
    position: relative;
  }

  bottom: 0;
  list-style: nome;

  li {
    @media (max-width: 768px) {
      padding: 1px;
    }
    padding: 5px;
  }

  .row {
    margin: 5px;
  }

  hr {
    background-color: #07a4ea;
    height: 2px;
  }
`;
