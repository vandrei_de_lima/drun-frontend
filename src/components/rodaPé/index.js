import React, { Component } from 'react';
import { Container } from './styled'
import { AiOutlineWhatsApp } from "react-icons/ai";
import { AiOutlineMail } from "react-icons/ai";


class RodaPé extends Component {


  render() {

    return (
      <Container>
        <div className="Corpo">
          <div className="row">
            <div className="Coluna" >
              <h4>Fale Conosco!</h4>
              <ul >
                <li>(47)997350464 <AiOutlineWhatsApp size={16} color="#07a4ea" /></li>
                <li>autoescoladrun@gmail.com <AiOutlineMail size={16} color="#07a4ea" /></li>
                <li>R. Pioneiro Andre Voltolini, 1060 - Nereu Ramos</li>
              </ul>
            </div>
            <hr />
            <div className="row">
              <p className="col-sm">
                &copy;{new Date().getFullYear()} Drun - Auto Escola | Todos os Direitos Reservados | Termos e Serviçoes | Privado
            </p>
            </div>
          </div>
        </div>

      </Container >
    )
  }
}
export default RodaPé;

