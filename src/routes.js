import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Home from './pages/home';
import Sobre from './pages/sobre';
import Info from './pages/Contato';
import Servico from './pages/serviços';


export default function Routes() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/Sobre" component={Sobre} />
        <Route path="/Informacao" component={Info} />
        <Route path="/Serviços" component={Servico} />

      </Switch>
    </BrowserRouter>
  );
}
